version: '2'
mpp-vars:
  name: soafee
  use_containers: true
  image_size: 25000000000
pipelines:
- mpp-import-pipelines:
    path: includes/build.ipp.yml
- name: rootfs
  build: name:build
  stages:
  - type: org.osbuild.kernel-cmdline
    options:
      root_fs_uuid:
        mpp-eval: rootfs_uuid
      kernel_opts:
        mpp-eval: ''' '' .join(kernel_opts)'
  - type: org.osbuild.rpm
    options:
      gpgkeys:
      - mpp-eval: centos_gpg_key
      - mpp-eval: redhat_gpg_key
      disable_dracut: true
    inputs:
      packages:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-depsolve:
          architecture: $arch
          ignore-weak-deps: true
          module-platform-id: $distro_module_id
          baseurl: $distro_baseurl/BaseOS/$arch/os/
          repos:
            mpp-join:
            - mpp-eval: image_repos
            - mpp-eval: extra_repos
          packages:
            mpp-join:
            - mpp-eval: image_rpms
            - mpp-eval: extra_rpms
            - - podman
              - podman-quadlet
              - containernetworking-plugins
              - curl
          excludes:
          - dracut-config-rescue
  - type: org.osbuild.skopeo
    inputs:
      images:
        type: org.osbuild.containers
        origin: org.osbuild.source
        mpp-resolve-images:
          images:
            - source: ghcr.io/autowarefoundation/autoware-universe
              tag: humble-latest-prebuilt
              name: localhost/autoware-universe
            - source: docker.io/tier4/scenario_simulator_v2
              tag: open_ad_kit-amd64-foxy
              name: localhost/scenario_simulator
    options:
      destination:
        type: containers-storage
        storage-path: /usr/share/containers/storage
  # AD KIT BEGING
  - type: org.osbuild.mkdir
    options:
      paths:
        - path: /etc/adkit
        - path: /etc/adkit/cyclonedds
        - path: /etc/adkit/map
        - path: /etc/adkit/simulator
        - path: /etc/adkit/simulator/bin
  - type: org.osbuild.sysctld
    options:
      filename: 60_cyclonedds.conf
      config:
        - key: net.ipv4.ipfrag_time
          value: "3"
        - key: net.ipv4.ipfrag_high_thresh
          value: "134217728"
        - key: net.core.rmem_max
          value: "2147483647"
        - key: net.core.rmem_default
          value: "8388608"
  - type: org.osbuild.copy
    inputs:
      # cylonedds
      cyclonedds:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: cyclonedds
          path: ../files/cyclonedds.xml
      # map data files
      lanelet2_map:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: lanelet2_map
          url: https://gitlab.com/autowarefoundation/autoware_reference_design/-/raw/main/docs/Appendix/Open-AD-Kit-Start-Guide/map/kashiwanoha/lanelet2_map.osm
      pointcloud_map:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: pointcloud_map
          url: https://gitlab.com/autowarefoundation/autoware_reference_design/-/raw/main/docs/Appendix/Open-AD-Kit-Start-Guide/map/kashiwanoha/pointcloud_map.pcd
      # service files
      ## apikit-api
      autoware_open_adkit_kube:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: autoware_open_adkit_kube
          path: ../files/quadlet/autoware-open-adkit.kube
      autoware_open_adkit_yaml:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: autoware_open_adkit_yaml
          path: ../files/quadlet/autoware-open-adkit.yml
    options:
      paths:
        # cyclonedds
        - from:
            mpp-format-string: input://cyclonedds/{embedded['cyclonedds']}
          to: tree:///etc/adkit/cyclonedds/cyclonedds.xml
        # map data files
        - from:
            mpp-format-string: input://lanelet2_map/{embedded['lanelet2_map']}
          to: tree:///etc/adkit/map/lanelet2_map.osm
        - from:
            mpp-format-string: input://pointcloud_map/{embedded['pointcloud_map']}
          to: tree:///etc/adkit/map/pointcloud_map.pcd
        - from:
            mpp-format-string: input://autoware_open_adkit_kube/{embedded['autoware_open_adkit_kube']}
          to: tree:///etc/containers/systemd/autoware-open-adkit.kube
        - from:
            mpp-format-string: input://autoware_open_adkit_yaml/{embedded['autoware_open_adkit_yaml']}
          to: tree:///etc/containers/systemd/autoware-open-adkit.yaml
   # AD KIT END
- mpp-import-pipelines:
    path: includes/image.ipp.yml
